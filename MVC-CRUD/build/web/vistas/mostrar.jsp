<%-- 
    Document   : listar
    Created on : 6/06/2023, 4:41:05 a. m.
    Author     : johnf
--%>

<%@page import="java.util.*"%>
<%@page import="Modelo.Persona"%>
<%@page import="ModeloDAO.PersonaDAO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <title>JSP Page</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
        <link rel="stylesheet" href="styles/style.css">
        <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;700&display=swap" rel="stylesheet">


    </head>
    <body>
        <div class="text-center">
            <h1>Tabla Personas</h1>
            <a href="Controlador?accion=agregar" class="btn btn-primary mb-3">Deseas añadir una nueva persona?</a>
        </div>
        <div class="table-container">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>NOMBRES</th>
                        <th>CORREO</th>
                        <th>ACCIONES</th>
                    </tr>
                </thead>


                <%
                    PersonaDAO dao = new PersonaDAO();
                    List<Persona> list = dao.listar();
                    Iterator<Persona> iter = list.iterator();
                    Persona per = null;

                    while (iter.hasNext()) {
                        per = iter.next();


                %>
                <tbody>


                    <tr>
                        <td><%=per.getId()%></td>
                        <td><%=per.getNombre()%></td>
                        <td><%=per.getApellido()%></td>

                        <td>
                            <a href="Controlador?accion=editar&id=<%= per.getId()%>" class="btn btn-primary">Editar</a>
                            <a href="Controlador?accion=eliminar&id=<%= per.getId()%>" class="btn btn-danger" >Eliminar</a>

                    </tr>
                    <% }%>
                </tbody>
            </table>
        </div>
    </div>
    <div>
        <footer class="footer">
            <div class="container d-flex justify-content-between align-items-center">
                <div>
                    <img src="assets/UFPS_Logo.png" alt="Icono 1" class="footer-icon">
                </div>
                <div>
                    Harold Ramirez Arboleda -- 1151577 -- Harold Ramirez Arboleda
                </div>
                <div>
                    <img src="assets/SIS_Logo.png" alt="Icono 2" class="footer-icon">
                </div>
            </div>
        </footer>
    </div>

    <!-- Agregar los scripts de Bootstrap y jQuery -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.5.4/dist/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <script src="js/cliente.js"></script>



</body>
</html>
