/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package Interfaces;

import Modelo.Persona;
import java.util.List;

/**
 *
 * @author johnf
 */
public interface CRUD {
    public List listar();
    public Persona list(int id);
    public boolean agregar(Persona per);
    public boolean editar(Persona per);
    public boolean eliminar(int id);
    
    
}
