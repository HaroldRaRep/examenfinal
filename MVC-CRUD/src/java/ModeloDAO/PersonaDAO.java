/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ModeloDAO;

import Config.Conexion;
import Interfaces.CRUD;
import Modelo.Persona;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author johnf
 */
public class PersonaDAO implements CRUD{

    Conexion cn=new Conexion();
    Connection con;
    PreparedStatement ps;
    ResultSet rs;
    Persona p=new Persona();
    @Override
    public List listar() {
        ArrayList<Persona>list=new ArrayList<>();
        String sql = "select * from usuario";
        try {
            con=cn.getConnection();
            ps=con.prepareStatement(sql);
            rs=ps.executeQuery();
            while(rs.next()){
                Persona per=new Persona();
                per.setId(rs.getInt("Id"));
                per.setNombre(rs.getString("Nombre"));
                per.setApellido(rs.getString("Correo"));
                list.add(per);
            }
        } catch (Exception e) {
            
        }
        return list;
    }

    @Override
    public Persona list(int id) {
        String sql = "select * from usuario where id="+id;
        try {
            con=cn.getConnection();
            ps=con.prepareStatement(sql);
            rs=ps.executeQuery();
            while(rs.next()){
                
                p.setId(rs.getInt("Id"));
                p.setNombre(rs.getString("Nombre"));
                p.setApellido(rs.getString("Correo"));
                
            }
        } catch (Exception e) {
            
        }
        return p;
        
    }

   @Override
    public boolean agregar(Persona per) {
        String sql = "insert into usuario(Nombre,Correo)values('" + per.getNombre() + "','" + per.getApellido() + "')";
        try {
            con = cn.getConnection();
            ps = con.prepareStatement(sql);
            ps.executeUpdate();

        } catch (Exception e) {
        }
        return false;

    }

    @Override
    public boolean editar(Persona per) {
        String sql = "update usuario set Nombre='"+per.getNombre()+"',Correo='"+per.getApellido()+"'where Id="+ per.getId();
        try {
            con=cn.getConnection();
            ps = con.prepareStatement(sql);
            ps.executeUpdate();
            
        } catch (Exception e) {
        }
        return false;
    }

    @Override
    public boolean eliminar(int id) {
        String sql = "delete from usuario where Id="+id;
        try {
            con=cn.getConnection();
            ps=con.prepareStatement(sql);
            ps.executeUpdate();
        } catch (Exception e) {
        }
        return false;
    }
    
}
