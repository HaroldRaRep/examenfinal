<%-- 
    Document   : index
    Created on : 6/06/2023, 4:40:15 a. m.
    Author     : johnf
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
        <link rel="stylesheet" href="styles/style.css">
        <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;700&display=swap" rel="stylesheet">
    </head>
    <body>
       <div class="text-center">
                <h1>Bienvenidos a mi Examen Final</h1>
                <a href="Controlador?accion=mostrar" class="btn btn-primary mb-3">Ir al CRUD</a>
            </div>


        <footer class="footer fixed-bottom">
            <div class="container d-flex justify-content-between align-items-center">
                <div>
                    <img src="assets/UFPS_Logo.png" alt="Icono 1" class="footer-icon">
                </div>
                <div>
                    Harold Ramirez Arboleda -- 1151577 -- Harold Ramirez Arboleda
                </div>
                <div>
                    <img src="assets/SIS_Logo.png" alt="Icono 2" class="footer-icon">
                </div>
            </div>
        </footer>


        <!-- Agregar los scripts de Bootstrap y jQuery -->
        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.5.4/dist/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
        <script src="js/cliente.js"></script>


    </body>
</html>
