# EXAMEN FINAL PROGRAMACION WEB 1151577 HAROLD RAMIREZ ARBOLEDA

Este es un proyecto trabajado con JSP el cual su objetivo es demostrar el manejo del modelo vista controlador (MVC) usando un CRUD(CREATE,READ,DELETE UPDATE)
#

Requisitos previos

NetBeans (versión 16)

XAMPP(version 3.3.0)

Importar Librerias

MySQL (mysql-connector-java-8.0.25)

#

Instalación
1.Clona o descarga este repositorio en tu máquina local.

2.Abre NetBeans y selecciona la opción "Abrir Proyecto".

3.Navega hasta la ubicación donde clonaste o descargaste el repositorio y selecciona la carpeta del proyecto.

4.Configura el servidor Tomcat en NetBeans.

5.Abre MySQL y crea una nueva base de datos.- >

CREATE TABLE `usuarioss`.`usuario` (`Id` INT(11) NOT NULL AUTO_INCREMENT , `Nombre` VARCHAR(50) NOT NULL , `Correo` VARCHAR(50) NOT NULL , PRIMARY KEY (`Id`)) ENGINE = InnoDB;

6.Ejecuta el archivo SQL proporcionado en el proyecto para crear las tablas necesarias en la base de datos.

7.Configura la conexión a la base de datos en el archivo de configuración database.properties ubicado en la carpeta src/main/resources.

8.Compila y ejecuta el proyecto en NetBeans.

#




¿Como Usar?


- Tenemos la pagina de inicio la cual tiene un boton simple que nos dirige a la pagina mostrar

![Captura de pantalla 1](https://i.ibb.co/MR1Kv1H/Pagina-Inicio.png)
#


Habiendo seleccionado el boton nos encontraremos con una interfaz la cual cuenta con una tabla que muestra los valores de la base de datos previamente conectada a Usuarios/usuario

En este caso tenemos 3 botones para seleccionar:
- El primero "Desea agregar mas personas ?" permite añadir otra persona y nos redirige a la pagina Agregar
- El segundo que es "editar" nos redirecciona a la pagina editar
-Y como ultimo eliminar el cual cumple la funcion de eliminar el valor de la tabla 



![Captura de pantalla 1](https://i.ibb.co/QjjzH67/Listar.png)


#
- Al momento de querer añadir un usuario hay que tener en cuenta 3 restricciones

1. Los espacios no pueden estar vacios
2. Nombre solo recibe caracteres alfabeticos
3. Correo solo recibe correos.

Despues al presionar el boton "Agregar personas" volveremos a la pagina mostrar donde se vera efectuado el cambio
![Captura de pantalla 2](https://i.ibb.co/2j0Bq3S/Agregar.png)
#
De igual manera al momento de modificar un usuario se tiene en cuenta las siguientes restricciones

1. Los espacios no pueden estar vacios
2. Nombre solo recibe caracteres alfabeticos
3. Correo solo recibe correos.

Despues al presionar el boton "Actualizar" volveremos a la pagina mostrar donde se vera efectuado el cambio

![Captura de pantalla 3](https://i.ibb.co/8NNJYJ5/Editar.png)
